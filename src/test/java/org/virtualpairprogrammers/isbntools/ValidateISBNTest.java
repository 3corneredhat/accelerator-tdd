package org.virtualpairprogrammers.isbntools;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValidateISBNTest {

    /* TDD Rules:
    * 1. Test the expected outcome of an example
    * 2. Don't pre-judge design... let your tests drive it
    * 3. Write the minimum code required to get tests to pass
    * 4. Each test should validate one single piece of logic
    * */
    @Test
    void checkAValidISBN() {

        // declare an object
        ValidateISBN validator = new ValidateISBN();
        // declare a method we expect to use
        // don't overthink the data type of variables
        boolean result = validator.checkISBN("0140449116");
        // If the result is true, pass; otherwise, fail
        assertTrue(result, "first value");
        result = validator.checkISBN("0140177396");
        assertTrue(result, "second value");

    }
    @Test
    void checkAnInvalidISBN() {


        ValidateISBN validator = new ValidateISBN();
        //use a different ISBN
        boolean result = validator.checkISBN("0140449117");
        // If the result is false, pass; otherwise, fail
        assertFalse(result);

    }
}